DROP DATABASE IF EXISTS lealpoints;
CREATE DATABASE lealpoints;

DROP DATABASE IF EXISTS lealpoints_unit_test;
CREATE DATABASE lealpoints_unit_test;

DROP DATABASE IF EXISTS lealpoints_functional_test;
CREATE DATABASE lealpoints_functional_test;

